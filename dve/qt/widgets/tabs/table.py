#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PyQt5.QtCore import Qt, QModelIndex, QSortFilterProxyModel
from PyQt5.QtWidgets import QTableView, QWidget, QPushButton, QVBoxLayout, QHBoxLayout, QAbstractItemView, \
    QAction, QDataWidgetMapper, QPlainTextEdit, QSplitter, QLineEdit

from dve.qt.delegates.table import TableDelegate
from dve.qt.models.table import TableModel

class TableTab(QWidget):

    def __init__(self, data, parent=None):
        super().__init__(parent=parent)

        self.tabs = parent

        # Make widgets ####################################

        self.splitter = QSplitter(orientation=Qt.Vertical, parent=self)

        self.table_view = QTableView(parent=self.splitter)

        self.btn_add_row = QPushButton("Add a row")

        # Splitter ########################################

        self.splitter.addWidget(self.table_view)

        # Mapped widgets ##################################

        num_mapped_widgets = sum([row["mapped"] for row in data.schema if "mapped" in row])

        self.mapped_widgets = {}
        if num_mapped_widgets > 0:
            self.edition_group = QWidget(parent=self.splitter)
            self.splitter.addWidget(self.edition_group)

            for row_index, row in enumerate(data.schema):
                if "mapped" in row and row["mapped"]:
                    if row["widget"] == "QLineEdit":
                        self.mapped_widgets[row_index] = QLineEdit(parent=self.edition_group)
                    elif row["widget"] == "QPlainTextEdit":
                        self.mapped_widgets[row_index] = QPlainTextEdit(parent=self.edition_group)

            self.set_mapped_widgets_enabled(False)

        # Set layouts #####################################

        self.vbox = QVBoxLayout()
        self.hbox = QHBoxLayout()

        self.vbox.addLayout(self.hbox)
        self.hbox.addWidget(self.splitter)
        self.vbox.addWidget(self.btn_add_row)
        self.setLayout(self.vbox)

        # Right widgets should be added in self.edition_group in children classes...

        # Set model #######################################

        self.table_source_model = TableModel(data, parent=self)  # TODO: right use of "parent" ?

        # Proxy model #####################################

        self.table_proxy_model = QSortFilterProxyModel(parent=self)  # TODO: right use of "parent" ?
        self.table_proxy_model.setSourceModel(self.table_source_model)

        self.table_view.setModel(self.table_proxy_model)
        #self.table_view.setModel(self.table_source_model)

        # Set the view ####################################

        self.table_view.setSelectionBehavior(QAbstractItemView.SelectRows)    # Select the full row when a cell is selected (See http://doc.qt.io/qt-5/qabstractitemview.html#selectionBehavior-prop )
        #self.table_view.setSelectionMode(QAbstractItemView.SingleSelection)  # Set selection mode. See http://doc.qt.io/qt-5/qabstractitemview.html#selectionMode-prop

        self.table_view.setAlternatingRowColors(True)
        self.table_view.setSortingEnabled(True)

        self.table_view.verticalHeader().setVisible(False)              # Hide the vertical header
        self.table_view.horizontalHeader().setStretchLastSection(True)  # http://doc.qt.io/qt-5/qheaderview.html#stretchLastSection-prop

        #self.table_view.setColumnHidden(0, True)  # ID column
        self.table_view.setColumnWidth(0, 50)

        for row_index, row in enumerate(data.schema):
            if (("mapped" in row) and (row["mapped"])) or (("hidden" in row) and (row["hidden"])): 
                self.table_view.setColumnHidden(row_index, True)

            elif "width" in row: 
                self.table_view.setColumnWidth(row_index, int(row["width"]))

        delegate = TableDelegate()
        self.table_view.setItemDelegate(delegate)

        # Set QDataWidgetMapper ###########################

        self.mapper = QDataWidgetMapper()
        self.mapper.setModel(self.table_proxy_model)          # WARNING: do not use `self.table_source_model` here otherwise the index mapping will be wrong!

        for row_index, row in enumerate(data.schema):
            if "mapped" in row and row["mapped"]:
                self.mapper.addMapping(self.mapped_widgets[row_index], row_index)
        
        #self.mapper.toFirst()                      # TODO: is it a good idea ?

        self.table_view.selectionModel().selectionChanged.connect(self.update_selection)

        # TODO: http://doc.qt.io/qt-5/qdatawidgetmapper.html#setCurrentModelIndex
        #self.table_view.selectionModel().currentRowChanged.connect(self.mapper.setCurrentModelIndex())

        # TODO: https://doc-snapshots.qt.io/qtforpython/PySide2/QtWidgets/QDataWidgetMapper.html#PySide2.QtWidgets.PySide2.QtWidgets.QDataWidgetMapper.setCurrentModelIndex
        #connect(myTableView.selectionModel(), SIGNAL("currentRowChanged(QModelIndex,QModelIndex)"),
        #mapper, SLOT(setCurrentModelIndex(QModelIndex)))

        # Set key shortcut ################################

        # see https://stackoverflow.com/a/17631703  and  http://doc.qt.io/qt-5/qaction.html#details

        # Add row action

        add_action = QAction(self.table_view)
        add_action.setShortcut(Qt.CTRL | Qt.Key_N)

        add_action.triggered.connect(self.add_row_btn_callback)
        self.table_view.addAction(add_action)

        # Delete action

        del_action = QAction(self.table_view)
        del_action.setShortcut(Qt.Key_Delete)

        del_action.triggered.connect(self.remove_row_callback)
        self.table_view.addAction(del_action)

        # Open web page action

        open_action = QAction(self.table_view)
        open_action.setShortcut(Qt.CTRL | Qt.Key_Space)

        open_action.triggered.connect(self.row_action_callback)
        self.table_view.addAction(open_action)

        # Set slots #######################################

        self.btn_add_row.clicked.connect(self.add_row_btn_callback)
        #self.btn_remove_row.clicked.connect(self.remove_row_callback)

        #self.table_view.setColumnHidden(1, True)

        # Set default sorted column

        self.table_view.sortByColumn(0, Qt.DescendingOrder)


    def update_selection(self, selected, deselected):
        sm = self.table_view.selectionModel()
        proxy_index = sm.currentIndex()
        has_selection = sm.hasSelection()

        if has_selection:
            self.set_mapped_widgets_enabled(True)
            self.mapper.setCurrentIndex(proxy_index.row())
        else:
            # When nothing is selected
            self.set_mapped_widgets_enabled(False)

    def set_mapped_widgets_enabled(self, enabled):
        if enabled:
            for column_index, widget in self.mapped_widgets.items():
                schema = self.table_source_model.schema[column_index]
                widget.setPlaceholderText(schema["header"])
                widget.setDisabled(False)
        else:
            for column_index, widget in self.mapped_widgets.items():
                if isinstance(widget, QLineEdit):
                    widget.setText("")
                elif isinstance(widget, QPlainTextEdit):
                    widget.setPlainText("")
                else:
                    raise Exception("Unknown type: " + type(widget))
                widget.setPlaceholderText("")
                widget.setDisabled(True)


    def add_row_btn_callback(self):
        parent = QModelIndex()                                   # More useful with e.g. tree structures

        row_source_index = 0                                           # Insert new rows to the begining
        #row_index = self.table_source_model.rowCount(parent)     # Insert new rows to the end

        self.table_source_model.insertRows(row_source_index, 1, parent)

        #index = self.table_source_model.index(row_index, 0)    # TODO
        #self.table_view.selectionModel().select(index, QItemSelectionModel.ClearAndSelect | QItemSelectionModel.Rows)    # TODO

    def remove_row_callback(self):
        parent = QModelIndex()                                   # More useful with e.g. tree structures

        # See http://doc.qt.io/qt-5/model-view-programming.html#handling-selections-in-item-views
        #current_index = self.table_view.selectionModel().currentIndex()
        #print("Current index:", current_index.row(), current_index.column())

        selection_proxy_index_list = self.table_view.selectionModel().selectedRows()    # selection_index_list is a list of QModelIndex
        #selection_source_index_list = [self.table_proxy_model.mapToSource(proxy_index) for proxy_index in selection_proxy_index_list]
        #selected_row_list = [source_index.row() for source_index in selection_source_index_list]
        selected_row_list = [source_index.row() for source_index in selection_proxy_index_list]

        #row_index = 0                                           # Remove the first row
        #row_index = self.table_source_model.rowCount(parent) - 1 # Remove the last row

        # WARNING: the list of rows to remove MUST be sorted in reverse order
        # otherwise the index of rows to remove may change at each iteration of the for loop!

        # TODO: there should be a lock mechanism to avoid model modifications from external sources while iterating this loop...
        #       Or as a much simpler alternative, modify the ItemSelectionMode to forbid the non contiguous selection of rows and remove the following for loop
        for row_index in sorted(selected_row_list, reverse=True):
            # Remove rows one by one to allow the removql of non-contiguously selected rows (e.g. "rows 0, 2 and 3")
            #success = self.table_source_model.removeRows(row_index, 1, parent)
            success = self.table_proxy_model.removeRows(row_index, 1, parent)
            if not success:
                raise Exception("Unknown error...")   # TODO

    def row_action_callback(self):
        pass
