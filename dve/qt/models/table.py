import datetime
import sys

from PyQt5.QtCore import Qt, QAbstractTableModel, QVariant, QModelIndex
from PyQt5.QtGui import QFont

from dve.data.table import PY_DATE_TIME_FORMAT, PY_DATE_FORMAT, PY_TIME_FORMAT, NONE_VALUE_STR

class TableModel(QAbstractTableModel):

    def __init__(self, data, parent=None):
        super().__init__(parent)

        self._data = data               # DON'T CALL THIS ATTRIBUTE "data", A QAbstractItemModel METHOD ALREADY HAVE THIS NAME (model.data(index, role)) !!!

    def rowCount(self, parent=None):
        return self._data.num_rows

    def columnCount(self, parent=None):
        return self._data.num_columns

    def data(self, index, role=Qt.DisplayRole):

        if role == Qt.DisplayRole:

            return self.dataDisplayRole(index)

        elif role == Qt.EditRole:

            return self.dataEditRole(index)

        elif role == Qt.FontRole:

            return self.dataFontRole(index)

        return QVariant()    # For others roles...


    def dataDisplayRole(self, index):
        """Méthode pour réduire les redondances dans les classes filles
        (pas besoins de réécrire le côde à executer pour tous les roles si on veut en changer qu'un...
        les autres sont directement hérités de la classe mère (i.e. cette classe))"""

        value = self._data.get_data(index.row(), index.column())

        if value is None:

            value = NONE_VALUE_STR

        elif (self.dtype[index.column()] in (datetime.datetime, datetime.date, datetime.time)) and ('format' in self.schema[index.column()]):

            value = value.strftime(format=self.schema[index.column()]['format'])

        elif self.dtype[index.column()] == datetime.datetime:

            value = value.strftime(format=PY_DATE_TIME_FORMAT)

        elif self.dtype[index.column()] == datetime.date:

            value = value.strftime(format=PY_DATE_FORMAT)

        elif self.dtype[index.column()] == datetime.time:

            value = value.strftime(format=PY_TIME_FORMAT)

        return value


    def dataEditRole(self, index):
        """Méthode pour réduire les redondances dans les classes filles
        (pas besoins de réécrire le côde à executer pour tous les roles si on veut en changer qu'un...
        les autres sont directement hérités de la classe mère (i.e. cette classe))"""

        value = self._data.get_data(index.row(), index.column())

        return value


    def dataFontRole(self, index):
        """Méthode pour réduire les redondances dans les classes filles
        (pas besoins de réécrire le côde à executer pour tous les roles si on veut en changer qu'un...
        les autres sont directement hérités de la classe mère (i.e. cette classe))"""

        style_dict = self._data.get_style(index.row(), index.column())

        qfont = QFont()

        if style_dict is not None:
            if ("bold" in style_dict) and (style_dict["bold"]):
                qfont.setBold(True)
            if ("italic" in style_dict) and (style_dict["italic"]):
                qfont.setItalic(True)
            if ("underline" in style_dict) and (style_dict["underline"]):
                qfont.setUnderline(True)
            if ("strikeout" in style_dict) and (style_dict["strikeout"]):
                qfont.setStrikeOut(True)

        return qfont


    def headerData(self, index, orientation=Qt.Horizontal, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Vertical:
                return str(index+1)
            elif orientation == Qt.Horizontal:
                return self.headers[index]
        return None

    def setData(self, index, value, role=Qt.EditRole):
        if role == Qt.EditRole:
            try:
                row_index, column_index = index.row(), index.column()

                if (value is None) and (('optional' not in self.schema[index.column()]) or (self.schema[index.column()]['optional'] == False)):

                    raise ValueError('{} cannot be set to "None"'.format(self.schema[index.column()]['header']))

                elif self.dtype[column_index] == float:

                    value = float(value)                      # Expect numerical values here... remove otherwise

                self._data.set_data(row_index, column_index, value)
            except Exception as e:
                print(e, file=sys.stderr)
                return False

            # The following lines are necessary e.g. to dynamically update the QSortFilterProxyModel
            # "When reimplementing the setData() function, dataChanged signal must be emitted explicitly"
            # http://doc.qt.io/qt-5/qabstractitemmodel.html#setData
            # TODO: check whether this is the "right" way to use the dataChanged signal

            self.dataChanged.emit(index, index, [Qt.EditRole])

        return True

    def flags(self, index):
        """Returns the item flags for the given `index`.

        See Also
        --------
        - http://doc.qt.io/qt-5/qabstractitemmodel.html#flags
        - http://doc.qt.io/qt-5/qt.html#ItemFlag-enum

        Parameters
        ----------
        index : QModelIndex
            TODO

        Returns
        -------
        ItemFlags
            The item flags for the given `index`.
        """
        if index.column() == 0:  # Special rule for the "ID" column : "IDs" are not editable
            return Qt.ItemIsSelectable | Qt.ItemIsEnabled
        else:
            return Qt.ItemIsSelectable | Qt.ItemIsEditable | Qt.ItemIsEnabled

    def insertRows(self, row, count, parent):
        """Inserts `count` rows into the model before the given `row`.

        Items in the new row will be children of the item represented by the `parent` model index.

        If `row` is 0, the rows are prepended to any existing rows in the `parent`.

        If `row` is `rowCount()`, the rows are appended to any existing rows in the `parent`.

        If `parent` has no children, a single column with `count` rows is inserted.

        Returns `True` if the rows were successfully inserted; otherwise returns `False`.

        See Also
        --------
        http://doc.qt.io/qt-5/qabstractitemmodel.html#insertRows

        Parameters
        ----------
        row : int
            TODO
        count : int
            TODO
        parent : QModelIndex, optional
            TODO

        Returns
        -------
        bool
            Returns `True` if the rows were successfully removed; otherwise returns `False`.
        """
        #try:
        parent = parent
        first_index = row
        last_index = first_index + count - 1

        self.beginInsertRows(parent, first_index, last_index)

        for i in range(count):
            self._data.insert_row(first_index)

        self.endInsertRows()
        #except Exception as e:
        #    print(e)
        #    return False

        return True

    def removeRows(self, row, count, parent):
        """Removes `count` rows starting with the given `row` under parent `parent` from the model.

        See Also
        --------
        http://doc.qt.io/qt-5/qabstractitemmodel.html#removeRows

        Parameters
        ----------
        row : int
            TODO
        count : int
            TODO
        parent : QModelIndex, optional
            TODO

        Returns
        -------
        bool
            Returns `True` if the rows were successfully removed; otherwise returns `False`.
        """
        # See http://doc.qt.io/qt-5/qabstractitemmodel.html#removeRows

        try:
            parent = parent
            first_index = row
            last_index = first_index + count - 1

            self.beginRemoveRows(parent, first_index, last_index)

            for i in range(count):
                self._data.remove_row(first_index)

            self.endRemoveRows()
        except Exception as e:
            print(e, file=sys.stderr)
            return False

        return True


    ###########################################################################

    def headerIndex(self, header_str):
        return self.headers.index(header_str)


    @property
    def dtype(self):
        return self._data.dtype

    @property
    def shape(self):
        return self._data.shape

    @property
    def headers(self):
        return self._data.headers


    @property
    def defaultValues(self):
        return self._data.default_values

    @property
    def defaultValuesDict(self):
        return self._data.default_values_dict

    @property
    def schema(self):
        return self._data.schema
