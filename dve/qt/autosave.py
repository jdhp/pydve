from PyQt5.QtCore import QTimer

class AutoSave:
    def __init__(self, timeout_minutes, status_bar=None):
        self._databases_and_data_structures = []

        self.timeout_minutes = timeout_minutes
        self.status_bar = status_bar

        self.timer = QTimer()
        self.timer.timeout.connect(self.save)


    def add(self, database, data_structure):
        self._databases_and_data_structures.append((database, data_structure))


    def start(self):
        self.timer.start(self.timeout_minutes * 1000 * 60)  # ms


    def save(self):
        if self.status_bar is not None:
            self.status_bar.showMessage("Saving data...", 2000)

        for database, data in self._databases_and_data_structures:
            database.save(data)
