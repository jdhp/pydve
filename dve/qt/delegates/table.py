#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QAction, QStyledItemDelegate, QDateTimeEdit, QTimeEdit, QDateEdit, QSpinBox, QComboBox, QDoubleSpinBox

from dve.data.table import PY_DATE_TIME_FORMAT, PY_DATE_FORMAT, PY_TIME_FORMAT, NONE_VALUE_STR


def datetime_python_format_to_qt_format(py_format_str):
    qt_format_str = py_format_str.replace("%Y", "yyyy").replace("%m", "MM").replace("%d", "dd").replace("%H", "HH").replace("%M", "mm").replace("%S", "ss")
    return qt_format_str


def datetime_qt_format_to_python_format(qt_format_str):
    py_format_str = qt_format_str.replace("yyyy", "%Y").replace("MM", "%m").replace("dd", "%d").replace("HH", "%H").replace("mm", "%M").replace("ss", "%S")
    return py_format_str


class TableDelegate(QStyledItemDelegate):

    def deleteActionCallback(self, editor, index):
        index.model().setData(index, None, Qt.EditRole)
        self.closeEditor.emit(editor)


    def createEditor(self, parent, option, index):
        """
        Crée le widget utilisé pour éditer la valeur d'une cellule

        Retourne un widget "vierge", i.e. ce n'est pas ici qu'on initialise la valeur du widget.
        En revanche, c'est ici qu'on peut définir les valeurs min/max acceptées, etc.

        https://doc.qt.io/qt-5/model-view-programming.html#providing-an-editor
        """
        model = index.model()
        source_model = index.model().sourceModel()
        proxy_index = index
        source_index = model.mapToSource(index)

        data_type = source_model.dtype[source_index.column()]
        data_schema = source_model.schema[source_index.column()]

        if data_type == datetime.datetime:

            if "format" in data_schema:
                qt_format_str = datetime_python_format_to_qt_format(data_schema["format"])
            else:
                qt_format_str = datetime_python_format_to_qt_format(PY_DATE_TIME_FORMAT)

            editor = QDateTimeEdit(parent=parent)
            #editor = QDateTimeEdit(parent=parent)
            editor.setDisplayFormat(qt_format_str)

            #editor.setMinimumDate(datetime.datetime(year=2018, month=1, day=1, hour=0, minute=0))
            #editor.setMaximumDate(datetime.datetime(year=2020, month=9, day=1, hour=18, minute=30))

        elif data_type == datetime.date:

            if "format" in data_schema:
                qt_format_str = datetime_python_format_to_qt_format(data_schema["format"])
            else:
                qt_format_str = datetime_python_format_to_qt_format(PY_DATE_FORMAT)

            editor = QDateEdit(parent=parent)
            editor.setCalendarPopup(True)
            editor.setDisplayFormat(qt_format_str)

        elif data_type == datetime.time:

            if "format" in data_schema:
                qt_format_str = datetime_python_format_to_qt_format(data_schema["format"])
            else:
                qt_format_str = datetime_python_format_to_qt_format(PY_TIME_FORMAT)

            editor = QTimeEdit(parent=parent)
            editor.setDisplayFormat(qt_format_str)

        elif data_type == int:

            editor = QSpinBox(parent=parent)

            if ("min_value" in data_schema) and ("max_value" in data_schema):
                min_value = data_schema["min_value"]
                max_value = data_schema["max_value"]
                editor.setRange(min_value, max_value)

        elif data_type == float:

            editor = QDoubleSpinBox(parent=parent)

            if "min_value" in data_schema:
                editor.setMinimum(data_schema["min_value"])
            if "max_value" in data_schema:
                editor.setMaximum(data_schema["max_value"])
            if "decimals" in data_schema:
                editor.setDecimals(data_schema["decimals"])
            if "step" in data_schema:
                editor.setSingleStep(data_schema["step"])

        elif (data_type == str) and ("values" in data_schema):

            editor = QComboBox(parent=parent)

            values_list = data_schema['values']
            editor.addItems(values_list)

        else:

            editor = QStyledItemDelegate.createEditor(self, parent, option, proxy_index)

        # setFrame(): tell whether the line edit draws itself with a frame.
        # If enabled (the default) the line edit draws itself inside a frame, otherwise the line edit draws itself without any frame.
        editor.setFrame(False)

        if ("optional" in data_schema) and (data_schema["optional"] == True):
            action = QAction(editor)                   # <-
            action.setShortcut(Qt.CTRL | Qt.Key_Delete)     # <-
            #action.setShortcut(Qt.Key_Delete)     # the keyevent for the suppr key is already catched by the editor thus it doesn't work...

            action.triggered.connect(lambda : self.deleteActionCallback(editor, index))  # <-
            editor.addAction(action)                   # <-

        return editor


    def setEditorData(self, editor, index):
        """
        Initialise la valeur du widget utilisé pour éditer la valeur d'une cellule.
        La donnée du modèle peut être récupérée via l'argument "index" (index.data(), index.model(), ...).

        Cette méthode:
        1. récupère la donnée du **modèle** (index.data(), ...)
        2. appèle la méthode approprée pour initialiser le widget editor (e.g. editor.setValue(value) si c'est un QSpinBox, ...)

        Mémo: model.data() -> editor.setValue()
        """
        model = index.model()
        source_model = index.model().sourceModel()
        proxy_index = index
        source_index = model.mapToSource(index)

        data_type = source_model.dtype[source_index.column()]
        data_schema = source_model.schema[source_index.column()]

        value = model.data(proxy_index, Qt.EditRole)

        if data_type == datetime.datetime:
            if value is None:
                value = datetime.datetime.now()          # TODO
            editor.setDateTime(value)     # value cannot be a string, it have to be a datetime...
        elif data_type == datetime.date:
            if value is None:
                value = datetime.datetime.now().date()
            editor.setDate(value)         # value cannot be a string, it have to be a date...
        elif data_type == datetime.time:
            if value is None:
                value = datetime.datetime.now().time()   # TODO
            editor.setTime(value)         # value cannot be a string, it have to be a time...
        elif data_type == int:
            if value is None:
                value = 0      # TODO
            editor.setValue(int(value))
        elif data_type == float:
            if value is None:
                value = 0.     # TODO
            editor.setValue(float(value))
        elif (data_type == str) and ("values" in data_schema):
            if value is None:
                value = ""
            editor.setCurrentText(value)
        else:
            return QStyledItemDelegate.setEditorData(self, editor, proxy_index)


    def setModelData(self, editor, model, index):
        """
        Submit editor's (widget) data to the *model*
        When the user has finished editing the value in the spin box,
        the view asks the delegate to store the edited value in the model by calling the setModelData() function.

        Cette méthode:
        1. récupère la donnée du **widget editor** avec la méthode appropriée (e.g. editor.value() si editor est un QSpinBox...)
        2. écrit la valeur dans le modèle au bon l'index: model.setData(...)

        https://doc.qt.io/qt-5/model-view-programming.html#submitting-data-to-the-model

        Mémo: editor.value() -> model.setDdata()
        """
        model = index.model()
        source_model = index.model().sourceModel()
        proxy_index = index
        source_index = model.mapToSource(index)

        data_type = source_model.dtype[source_index.column()]
        data_schema = source_model.schema[source_index.column()]

        if data_type in (datetime.datetime, datetime.date, datetime.time):
            editor.interpretText()
            str_value = editor.text()

            if "format" in data_schema:
                py_format_str = data_schema["format"]
            elif data_type == datetime.datetime:
                py_format_str = PY_DATE_TIME_FORMAT
            elif data_type == datetime.date:
                py_format_str = PY_DATE_FORMAT
            elif data_type == datetime.time:
                py_format_str = PY_TIME_FORMAT

            dt_value = datetime.datetime.strptime(str_value, py_format_str)

            if data_type == datetime.date:
                dt_value = dt_value.date()
            elif data_type == datetime.time:
                dt_value = dt_value.time()

            model.setData(proxy_index, dt_value, Qt.EditRole)
        elif data_type in (int, float):
            editor.interpretText()
            value = editor.value()
            model.setData(proxy_index, value, Qt.EditRole)
        elif (data_type == str) and ("values" in data_schema):
            value = editor.currentText()
            model.setData(proxy_index, value, Qt.EditRole)
        else:
            return QStyledItemDelegate.setModelData(self, editor, model, proxy_index)


    def updateEditorGeometry(self, editor, option, index):
        """
        It is the responsibility of the delegate to manage the editor's geometry.
        The geometry must be set when the editor is created, and when the item's size or position in the view is changed.
        Fortunately, the view provides all the necessary geometry information inside a view option object.
 
        In this case, we just use the geometry information provided by the view option in the item rectangle.
        A delegate that renders items with several elements would not use the item rectangle directly.
        It would position the editor in relation to the other elements in the item.

        https://doc.qt.io/qt-5/model-view-programming.html#updating-the-editor-s-geometry
        """
        model = index.model()
        source_model = index.model().sourceModel()
        proxy_index = index
        source_index = model.mapToSource(index)

        data_type = source_model.dtype[source_index.column()]
        data_schema = source_model.schema[source_index.column()]

        if (data_type in (datetime.datetime, datetime.date, int, float)) or ("values" in data_schema):
            editor.setGeometry(option.rect)
        else:
            return QStyledItemDelegate.updateEditorGeometry(self, editor, option, proxy_index)
