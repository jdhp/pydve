#!/usr/bin/env python3
# -*- coding: utf-8 -*-

PY_DATE_TIME_FORMAT = r"%Y-%m-%d %H:%M:%S"  # TODO
PY_DATE_FORMAT = r"%Y-%m-%d"  # TODO
PY_TIME_FORMAT = r"%H:%M:%S"  # TODO

ID_COLUMN_LABEL = "ID"    # TODO

NONE_VALUE_STR = "n/a"    # TODO

import datetime

class TableDataStructure:
    """Cette classe est une structure de données de tableau avec header et schema attaché (plutôt qu'une simple liste imbriquée).

    Dans l'esprit, c'est une sorte de DataFrame Pandas mais très simplifié."""

    def __init__(self, data_schema):
        self._data_schema = data_schema
        self._data = []
        self._last_id = 0

    # TODO: redefine [x,y] operator (as in numpy)
    def get_data(self, row_index, column_index):
        return self._data[row_index][column_index]

    # TODO: redefine [x,y] operator (as in numpy)
    def set_data(self, row_index, column_index, value):

        if not (isinstance(value, self.dtype[column_index]) or (value is None) and ('optional' in self.schema[column_index]) and (self.schema[column_index]['optional'])):
            raise ValueError("Error at row {} column {} with value {}. Expect {} instance. Got {}".format(row_index, column_index, value, self.dtype[column_index], type(value)))

        id_index = self.headers.index(ID_COLUMN_LABEL)
        if column_index == id_index:
            self._last_id = max(self._last_id, value)

        self._data[row_index][column_index] = value


    def get_style(self, row_index, column_index):
        return None


    def append(self, row):
        row_index = self.num_rows - 1
        self.insert_row(row_index, row=row)


    def append_row_data(self, row_data_dict):
        row_index = self.num_rows - 1
        row = list(self.default_values)

        self._data.insert(row_index, [None for i in range(self.num_columns)])

        for column_index in range(self.num_columns):
            column_label = self.headers[column_index]
            if column_label in row_data_dict:
                value = row_data_dict[column_label]
            else:
                value = row[column_index]
            self.set_data(row_index, column_index, value)


    def insert_row(self, row_index, row=None):
        if row is None:
            row = list(self.default_values)

        self._data.insert(row_index, [None for i in range(self.num_columns)])

        for column_index in range(self.num_columns):
            self.set_data(row_index, column_index, row[column_index])


    def remove_row(self, index):
        if self.num_rows > 0:
            _removed = self._data.pop(index)

    #def describe(self, index):
    #    if index == 0:
    #        return {"header": ID_COLUMN_LABEL, "default_value": int(self._last_id + 1), "dtype": int}
    #    else:
    #        return self._data_schema[index]


    def get_rows(self, column_index, value):
        row_index_list = []

        for row_index in range(self.num_rows):
            if value == self.get_data(row_index=row_index, column_index=column_index):
                row_index_list.append(row_index)

        return row_index_list


    def to_dict(self, json_format=False):
        data_dict = {}

        for row_index in range(self.num_rows):
            row_dict = {}

            for col_label, dtype, col_index in zip(self.headers, self.dtype, range(self.num_columns)):
                value = self.get_data(row_index=row_index, column_index=col_index)

                if value is not None:
                    if (dtype == datetime.datetime) and (json_format is True):
                        value = value.strftime(format=PY_DATE_TIME_FORMAT)
                    elif (dtype == datetime.date) and (json_format is True):
                        value = value.strftime(format=PY_DATE_FORMAT)
                    elif (dtype == datetime.time) and (json_format is True):
                        value = value.strftime(format=PY_TIME_FORMAT)

                row_dict[col_label] = value

            row_id = row_dict.pop(ID_COLUMN_LABEL)
            data_dict[row_id] = row_dict

        return data_dict
    
    @property
    def num_rows(self):
        return len(self._data)

    @property
    def num_columns(self):
        return len(self.headers)

    @property
    def shape(self):
        return (self.num_rows, self.num_columns)

    @property
    def headers(self):  # TODO
        return [ID_COLUMN_LABEL] + [row['header'] for row in self._data_schema]

    @property
    def default_values(self):  # TODO
        return [int(self._last_id + 1)] + [row['default_value'] for row in self._data_schema]

    @property
    def default_values_dict(self):  # TODO
        return {row['header']: row['default_value'] for row in self._data_schema}

    @property
    def dtype(self):  # TODO
        #return [int] + [row['dtype'] if not isinstance(row['dtype'], (tuple, list)) else str for row in self._data_schema]
        return [int] + [row['dtype'] for row in self._data_schema]

    @property
    def schema(self):
        return [{"header": ID_COLUMN_LABEL, "default_value": int(self._last_id + 1), "dtype": int}] + self._data_schema