#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime
import json
import os
import shutil

from dve.data.table import TableDataStructure, ID_COLUMN_LABEL, PY_DATE_TIME_FORMAT, PY_DATE_FORMAT, PY_TIME_FORMAT
from dve.io.lock import lock_path, unlock_path

class TableDataBase:

    def __init__(self, data_schema, file_name):
        self._data_schema = data_schema
        self._file_name = file_name
        lock_path(self.path)


    def __del__(self):
        unlock_path(self.path)


    def load(self, init_data_structure=None):
        """Load the JSON database from a file."""

        json_data_dict = {}

        try:
            with open(self.path, "r") as fd:
                json_data_dict = json.load(fd)
        except FileNotFoundError:
            pass

        return self.load_dict(json_data_dict, init_data_structure)


    def load_dict(self, json_data_dict, init_data_structure=None):
        """Load the JSON database from a dictionary (useful for unit tests)."""

        if init_data_structure is not None:
            data = init_data_structure
        else:
            data = TableDataStructure(self._data_schema)

        for item_id, item_dict in json_data_dict.items():
            item_list = []

            for col_label, dtype in zip(data.headers, data.dtype):
                if col_label == ID_COLUMN_LABEL:
                    value = int(item_id)
                #elif col_label == "Priority":   # Temporary uncomment this line to introduce new fields in existing data
                #    value = 0.                  # Temporary uncomment this line to introduce new fields in existing data
                else:
                    value = item_dict[col_label]

                    if value is not None:
                        if dtype == datetime.datetime:
                            value = datetime.datetime.strptime(value, PY_DATE_TIME_FORMAT)
                        elif dtype == datetime.date:
                            value = datetime.datetime.strptime(value, PY_DATE_FORMAT).date()
                        elif dtype == datetime.time:
                            value = datetime.datetime.strptime(value, PY_TIME_FORMAT).time()

                item_list.append(value)

            data.append(item_list)

        return data


    def save(self, data):
        """Save the JSON database."""

        # Use a dict structure to have items sorted by ID automatically by the JSON parser (for some strange reason, the first and the last items are switched when a list is used)
        json_data_dict = data.to_dict(json_format=True)

        shutil.copyfile(self.path, self.path + ".bak")    # A backup is made before we try to write data (to avoid data loss)

        with open(self.path, "w") as fd:
            json.dump(json_data_dict, fd, sort_keys=True, indent=4)


    @property
    def path(self):
        home_path = os.path.expanduser("~")                 # TODO: does it work on Unix only ?
        file_path = os.path.join(home_path, self._file_name)
        return file_path
