#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains unit tests for the "dve.io.table" module.
"""

import tempfile
from dve.io.table import TableDataBase
from dve.data.table import TableDataStructure

def test_load_table_from_file():

    # Create a temporary file using a context manager
    with tempfile.NamedTemporaryFile() as tf:
        file_path = tf.name
        table_data_schema = [
                {"header": "Label",       "default_value": "", "dtype": str, "mapped": False,},
                {"header": "Description", "default_value": "", "dtype": str, "mapped": True, "widget": "QPlainTextEdit"}
            ]

        table_database = TableDataBase(table_data_schema, file_path)

        data = TableDataStructure(table_data_schema)

        table_database.save(data)

        tf.seek(0)
        data = table_database.load()

    # The file is now closed and removed


def test_load_table():

    table_data_schema = [
            {"header": "Label",       "default_value": "", "dtype": str, "mapped": False,},
            {"header": "Description", "default_value": "", "dtype": str, "mapped": True, "widget": "QPlainTextEdit"}
        ]

    table_database = TableDataBase(table_data_schema, "foo.tmp")   # TODO

    json_data_dict = {
        "1": {
            "Label": "foo",
            "Description": "bar"
        },
        "3": {
            "Label": "foo",
            "Description": "bar"
        }
    }

    data = table_database.load_dict(json_data_dict)

    #assert data.to_dict().keys() == json_data_dict.keys()
